package es.vidal.actividad2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Actividad2 {

    public static final Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {
        ArrayList<String> listadoAlumnos = new ArrayList<>();

        listadoAlumnos.add("Carlos");
        listadoAlumnos.add("Jordi");
        listadoAlumnos.add("Jose");
        listadoAlumnos.add("Sergio");
        listadoAlumnos.add("Andrés");

        Iterator<String> iterador2 = listadoAlumnos.iterator();

        while (iterador2.hasNext()){
            System.out.print(iterador2.next() + " ");
            System.out.println();
        }
        mostrarSiEstaCompanyero(listadoAlumnos);
    }

    public static void mostrarSiEstaCompanyero(ArrayList<String> alumnos){
        String alumno = obtenerAlumno();
        if (alumnos.contains(alumno)){
            System.out.println("Sí que es compañero");
        }else {
            System.out.println("No es compañero");
        }
    }

    public static String obtenerAlumno(){
        System.out.println("Introduce un Alumno");
        return teclado.next();

    }
}
