package es.vidal.actividad8;

import java.time.LocalDateTime;

public class Entrada {

    private final String descripcion;

    private final LocalDateTime fecha;

    public Entrada(String descripcion) {
        this.descripcion = descripcion;
        fecha = LocalDateTime.now();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Entrada){
            Entrada tmpEntrada = (Entrada) obj;

            return this.descripcion.equals(tmpEntrada.descripcion);
        }else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Fecha: " + fecha + " Descripción: " + descripcion;
    }
}
