package es.vidal.actividad8;

import java.util.Arrays;
import java.util.HashSet;

public class BitacoraInteligente extends Bitacora{

    private final HashSet<String> palabrasProhibidas = new HashSet<>();

    private final HashSet<String> spam = new HashSet<>();

    public BitacoraInteligente(String nombre, String ... palabras) {
        super(nombre);
        this.palabrasProhibidas.addAll(Arrays.asList(palabras));
    }

    public int getSpam() {
        return spam.size();
    }

    public String[] dividirDescripcion(String descripcion){
        return descripcion.split(" ");
    }

    @Override
    public boolean registrarEntrada(String descripcion) {
        String[] palabras = dividirDescripcion(descripcion);
        for (String palabra : palabras) {
            if (palabrasProhibidas.contains(palabra)) {
                spam.add(descripcion);
                return false;
            }
        }
       return super.registrarEntrada(descripcion);
    }
}
