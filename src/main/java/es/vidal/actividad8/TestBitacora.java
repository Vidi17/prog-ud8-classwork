package es.vidal.actividad8;

import java.util.LinkedList;

public class TestBitacora {
    public static void main(String[] args) {

        Bitacora bitacora1 = new Bitacora("basica1");

        BitacoraInteligente inteligente1 = new BitacoraInteligente("inteligente1", "facebook"
                , "thepiratebay");

        BitacoraInteligente inteligente2 = new BitacoraInteligente("inteligente2", "foto", "descargar");

        LinkedList<Bitacora> listaBitacoras = new LinkedList<>();

        listaBitacoras.add(bitacora1);
        listaBitacoras.add(inteligente1);
        listaBitacoras.add(inteligente2);

        anyadirEntradas(listaBitacoras);

        mostrarEntradas(listaBitacoras);

        System.out.println("El número de palabras registradas en spam en la bitacora inteligente 1 es de " + inteligente1.getSpam());
        System.out.println("El número de palabras registradas en spam en la bitacora inteligente 2 es de " + inteligente2.getSpam());

    }

    public static void mostrarEntradas(LinkedList<Bitacora> listaBitacoras){
        for (Bitacora listaBitacora : listaBitacoras) {
            System.out.println("Mostrar entradas de la Bitácora: " + listaBitacora.getNombre());
            System.out.println();
            System.out.println(listaBitacora.getEntradas());
            System.out.println();
        }
    }

    public static void anyadirEntradas(LinkedList<Bitacora> listaBitacoras){
        for (Bitacora listaBitacora : listaBitacoras) {
            System.out.println("Añadir entradas en la Bitácora: " + listaBitacora.getNombre());
            System.out.println();
            registrarVariasEntradas(listaBitacora);
        }
    }

    public static void registrarVariasEntradas(Bitacora listaBitacora){
        if (listaBitacora.registrarEntrada("Se ha producido un error en el servicio")){
            System.out.println("La entrada ha sido añadida correctamente");
        }else {
            System.out.println("La entrada no ha podido ser añadida");
        }

        if (listaBitacora.registrarEntrada("Puedes descargar el driver desde thepiratebay")){
            System.out.println("La entrada ha sido añadida correctamente");
        }else {
            System.out.println("La entrada no ha podido ser añadida");
        }

        if (listaBitacora.registrarEntrada("Las fotos están publicadas en facebook")){
            System.out.println("La entrada ha sido añadida correctamente");
        }else {
            System.out.println("La entrada no ha podido ser añadida");
        }
        System.out.println();
    }
}
