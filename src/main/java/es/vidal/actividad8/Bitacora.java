package es.vidal.actividad8;

import java.util.LinkedList;

public class Bitacora {

    private final String nombre;

    private final LinkedList<Entrada> entradas = new LinkedList<>();

    private int numeroEntradas;

    public Bitacora(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public LinkedList<Entrada> getEntradas() {
        return entradas;
    }

    public boolean registrarEntrada(String descripcion){

        entradas.add(new Entrada(descripcion));
        numeroEntradas++;
        return true;
    }
}
