package es.vidal.actividad3;

import java.util.ArrayList;
import java.util.Random;

public class Actividad3 {
    public static void main(String[] args) {
        ArrayList<Integer> listadoNumeros = new ArrayList<>();

        Random aleatorio = new Random();

        for (int i = 0; i < 50; i++) {
            listadoNumeros.add(aleatorio.nextInt(101));
        }

        System.out.printf("La suma total es %d", mostrarSuma(listadoNumeros));
        System.out.println();
        System.out.printf("La media total es %d", mostrarMedia(listadoNumeros));
        System.out.println();
        System.out.printf("El número más alto es %d", mostrarMaximo(listadoNumeros));
        System.out.println();
        System.out.printf("El número más pequeño es %d", mostrarMinimo(listadoNumeros));
        System.out.println();
    }

    public static int mostrarMaximo(ArrayList<Integer> numeros){
        int numeroMaximo = numeros.get(0);
        for (Integer numero : numeros) {
            if (numero > numeroMaximo){
                numeroMaximo = numero;
            }
        }
        return numeroMaximo;
    }

    public static int mostrarMinimo(ArrayList<Integer> numeros){
        int numeroMinimo = numeros.get(0);
        for (Integer numero : numeros) {
            if (numero < numeroMinimo){
                numeroMinimo = numero;
            }
        }
        return numeroMinimo;
    }

    public static int mostrarSuma(ArrayList<Integer> numeros){
        int sumaTotal = 0;
        for (Integer numero : numeros) {
            sumaTotal += numero;
        }
        return sumaTotal;
    }

    public static int mostrarMedia(ArrayList<Integer> numeros){
        int totalNumeros = numeros.size();
        int mediaTotal = 0;
        for (Integer numero : numeros) {
            mediaTotal += numero;
        }
        return mediaTotal / totalNumeros;
    }
}
