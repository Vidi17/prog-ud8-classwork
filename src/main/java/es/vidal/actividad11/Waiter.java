package es.vidal.actividad11;

import java.util.LinkedList;
import java.util.Scanner;

public class Waiter {

    private final Scanner keyboard = new Scanner(System.in);

    private final Menu menu;

    private Order order;

    public Waiter(Menu menu) {
        this.menu = menu;
    }

    public void setOrder(){
        order = createOrder();
    }

    public Order getOrder(){
        return order;
    }

    public Order createOrder(){
        Order order = new Order(getClientName(), getDate());
        menu.listAll();
        order.setProductList(getProductListForOrder());

        return order;
    }

    public LinkedList<Product> getProductListForOrder(){
        LinkedList<Product> orderProducts = new LinkedList<>();
        Product productToAdd;
        boolean wantMore = true;
        do {
            productToAdd = getProductForOrder();
            if (productToAdd != null){
                addQuantity(orderProducts, productToAdd);
                System.out.println("Añadido");
                System.out.println("¿Quiere introducir más productos en su pedido? [Si][No]");
                String addMore = keyboard.next();
                if (addMore.equalsIgnoreCase("No")){
                    wantMore = false;
                }
            }else{
                wantMore = false;
            }
        }while (wantMore);
        return orderProducts;
    }

    public void addQuantity(LinkedList<Product> orderProducts, Product productToAdd){
        System.out.println("¿Cuantas unidades quieres añadir de este producto?:");
        int quantitySelected = IntegerValidator.getInteger();
        for (int i = 0; i < quantitySelected; i++) {
            orderProducts.add(productToAdd);
        }
    }

    public Product getProductForOrder(){
        System.out.println("Introduzca el código del producto que desea añadir o introduzca fin para finalizar:");
        boolean validProductName;
        Product productSearched = null;
        String productToSearch;
        do{
            do {
                validProductName = keyboard.hasNextInt();
                productToSearch = keyboard.next();
                if (productToSearch.equalsIgnoreCase("fin")) {
                    return null;
                }else if (!validProductName){
                    productSearched = menu.searchProduct(productToSearch);
                    validProductName = true;
                }else {
                    System.out.println("Introduzca un código de producto válido o introduzca fin para finalizar:");
                    validProductName = false;
                }
            }while (!validProductName);

            if (productSearched == null){
                System.out.println("Introduzca un código de producto válido o introduzca fin para finalizar:");
            }
        }while (productSearched == null);
        return productSearched;
    }

    public Fecha getDate(){
        System.out.println("Introduzca la fecha actual en formato dd/mm/yyyy:");
        boolean validDate;
        Fecha date = new Fecha();
        do {
            validDate = keyboard.hasNextInt();
            if (validDate){
                System.out.println("Introduzca una Fecha válida:");
                keyboard.nextLine();
                validDate = false;
            }else {
                validDate = true;
                try {
                    date = new Fecha(keyboard.next());
                }catch (NumberFormatException ex){
                    System.out.println("Introduzca una fecha válida:");
                    keyboard.nextLine();
                    validDate = false;
                }
            }
        }while (!validDate );
        return date;
    }

    public String getClientName(){
        System.out.println("Introduzca su nombre:");
        boolean validClientName;
        do {
            validClientName = keyboard.hasNextInt();
            if (validClientName) {
                System.out.println("Introduzca un nombre válido:");
                keyboard.nextLine();
                validClientName = false;
            }else {
                validClientName = true;
            }
        }while (!validClientName);
        return keyboard.next();
    }

}
