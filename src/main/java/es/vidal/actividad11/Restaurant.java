package es.vidal.actividad11;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;

import java.util.LinkedList;
import java.util.Scanner;

public class Restaurant {

    private final Scanner keyboard = new Scanner(System.in);

    LinkedList<Order> orderList = new LinkedList<>();

    Waiter waiter = new Waiter(new Menu());

    public void addOrder(){
        waiter.setOrder();
        orderList.add(waiter.getOrder());
        orderList.get(orderList.size() - 1).setIdOrder("o" + orderList.size());
    }

    public void markOrder(){
        if (orderList.isEmpty()) {
            System.out.println("Ahora mismo no hay nigún pedido en la lista. Por favor añade un pedido.\n");
        }else {
            listPendingOrders();
            Order orderToMark;
            do {
                orderToMark = getOrderToMark();
                if (orderToMark!= null){
                    System.out.println(orderToMark);
                    System.out.println("Su pedido ha sido servido correctamente");
                }
            }while (orderToMark != null);
        }
    }

    public void showOrder(){
        if (orderList.isEmpty()){
            System.out.println("Ahora mismo no hay nigún pedido en la lista. Por favor añade un pedido.\n");
        }else {
            listAllOrders();
            Order orderToShow;
            do {
                orderToShow = getOrderToShow();
                if (orderToShow != null){
                    System.out.println(orderToShow);
                }
            }while (orderToShow != null);
        }
    }

    public Order searchOrder(String code){
        for (Order order: orderList) {
            if (order.getIdOrder().equals(code)){
                System.out.print(order);
                return order;
            }
        }
        return null;
    }

    public Order searchOrderToMark(String code){
        for (Order order: orderList) {
            if (order.getIdOrder().equals(code)){
                order.setServed(true);
                return order;
            }
        }
        return null;
    }

    public Order getOrderToShow(){
        System.out.println("Introduzca el código del pedido que desea visualizar o introduzca fin para finalizar:");
        boolean validIdOrder;
        Order orderSearched = null;
        String orderToSearch;
        do {
            validIdOrder = keyboard.hasNextInt();
            orderToSearch = keyboard.next();
            if (orderToSearch.equalsIgnoreCase("fin")) {
                return null;
            }else if (!validIdOrder){
                orderSearched = searchOrder(orderToSearch);
                validIdOrder = true;
            }else {
                System.out.println("Introduzca un código de pedido válido o introduzca fin para finalizar:");
                validIdOrder = false;
            }
        }while (!validIdOrder);
        return orderSearched;
    }

    public Order getOrderToMark(){
        System.out.println("Introduzca el código del pedido que desea marcar o introduzca fin para finalizar:");
        boolean validIdOrder;
        Order orderSearched = null;
        String orderToMark;
        do {
            validIdOrder = keyboard.hasNextInt();
            orderToMark = keyboard.next();
            if (orderToMark.equalsIgnoreCase("fin")) {
                return null;
            }else if (!validIdOrder){
                orderSearched = searchOrderToMark(orderToMark);
                validIdOrder = true;
            }else {
                System.out.println("Introduzca un código de pedido válido o introduzca fin para finalizar:");
                validIdOrder = false;
            }
        }while (!validIdOrder);
        return orderSearched;
    }

    public void listAllOrders(){
        System.out.println(createTable().render(110));
        System.out.println();
    }

    public void listPendingOrders(){
        System.out.println(createPendingTable().render(110));
        System.out.println();
    }

    public AsciiTable createTable(){
        AsciiTable asciiTable = new AsciiTable();
        asciiTable.addRule();
        asciiTable.addRow("Código", "Nombre", "Fecha", "Estado");
        asciiTable.addRule();
        for (Order order: orderList) {
            if (order.isServed()){
                asciiTable.addRow(order.getIdOrder(), order.getClientName(), order.getOrderDate()
                        , "Servido");
            }else {
                asciiTable.addRow(order.getIdOrder(), order.getClientName(), order.getOrderDate()
                        , "No servido");
            }
            asciiTable.addRule();
        }
        asciiTable.addRow("#", "#", "#", "#");
        asciiTable.setTextAlignment(TextAlignment.RIGHT);
        asciiTable.addRule();
        return asciiTable;
    }

    public AsciiTable createPendingTable(){
        AsciiTable asciiTable = new AsciiTable();
        asciiTable.addRule();
        asciiTable.addRow("Código", "Nombre", "Fecha", "Estado");
        asciiTable.addRule();
        for (Order order: orderList) {
            if (!order.isServed()){
                asciiTable.addRow(order.getIdOrder(), order.getClientName(), order.getOrderDate()
                        , "No servido");
                asciiTable.addRule();
            }
        }
        asciiTable.addRow("#", "#", "#", "#");
        asciiTable.setTextAlignment(TextAlignment.RIGHT);
        asciiTable.addRule();
        return asciiTable;
    }
}
