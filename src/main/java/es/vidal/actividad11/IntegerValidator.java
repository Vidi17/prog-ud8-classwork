package es.vidal.actividad11;

import java.util.Scanner;

public class IntegerValidator {

    public static int getInteger() {
        boolean isInteger;
        Scanner keyboard = new Scanner(System.in);
        do {
            if (!keyboard.hasNextInt()) {
                System.out.print("Debe introducir un entero: ");
                keyboard.nextLine();
                isInteger = false;
            }else {
                isInteger = true;
            }
        }while (!isInteger);
        return keyboard.nextInt();
    }
}
