package es.vidal.actividad11;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;

import java.text.DecimalFormat;
import java.util.LinkedList;

public class Order {

    private String idOrder;

    private LinkedList<Product> productList;

    private final Fecha orderDate;

    private final String clientName;

    private boolean served;

    private double orderPrice;

    public Order(String clientName, Fecha orderDate) {
        this.orderDate = orderDate;
        this.clientName = clientName;
    }

    public String getIdOrder() {
        return idOrder;
    }

    public Fecha getOrderDate() {
        return orderDate;
    }

    public boolean isServed() {
        return served;
    }

    public String getClientName() {
        return clientName;
    }

    public void setOrderPrice(){
        for (Product product: productList) {
            orderPrice += product.getSalePrice();
        }
    }

    public void setIdOrder(String idOrder){
        this.idOrder = idOrder;
    }

    public void setServed(boolean isServed){
        served = isServed;
    }

    public void setProductList(LinkedList<Product> productList){
        this.productList = productList;
        setOrderPrice();
    }

    public AsciiTable getTable(){
        DecimalFormat df = new DecimalFormat("0.00");
        AsciiTable asciiTable = new AsciiTable();
        asciiTable.addRule();
        asciiTable.addRow("Código", null, null, null, "Descripción", "Precio", "Descuento");
        asciiTable.addRule();
        for (Product product: productList) {
            asciiTable.addRow(product.getCodProduct(), null, null, null, product.getDescription()
                    , product.getBasePrice() + "€ ", product.getDiscount() + "%");
            asciiTable.addRule();
        }
        asciiTable.addRow(null, null, null, null, null, null, "Importe Total: " + df.format(orderPrice) + "€");
        asciiTable.addRule();
        asciiTable.setTextAlignment(TextAlignment.RIGHT);
        asciiTable.addRow("#", "#", "#", "#", "#", "#", "#");
        asciiTable.addRule();
        return asciiTable;
    }

    @Override
    public String toString() {

        return "Nombre del Cliente: " + clientName + " Fecha del pedido: " + orderDate + "\n" +
                getTable().render(110) + "\n";
    }
}
