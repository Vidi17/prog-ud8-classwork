package es.vidal.actividad11;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.skb.interfaces.transformers.textformat.TextAlignment;
import java.util.HashSet;


public class Menu {

    HashSet<Product> productList = new HashSet<>();

    public Menu() {
        productList.add(new Product("lechuga, tomate, mayonesa", 1.38, 0));
        productList.add(new Product("HUEVO DURO lechuga, tomate y mayonesa", 1.38, 0));
        productList.add(new Product("VEGETAL CON QUESO lechuga, tomate y queso", 1.38, 0));
        productList.add(new Product("Burguer, bacon ahumado, cebolla crujiente y allioli", 1.38
                , 0));
        productList.add(new Product("Pollo, bacon ahumado y salsa brava", 1.38, 0));
        productList.add(new Product("Pollo kebab, cebolla, pimiento verde y mayonesa", 1.38
                , 0));
        productList.add(new Product("CUATRO QUESOS: Queso ibérico, queso brie, queso de cabra y crema de queso"
                , 1.38, 0));
        productList.add(new Product("CAPRESE: Jamón gran reserva, queso mozzarella, tomate y pesto"
                , 1.38, 0));
        productList.add(new Product("Pulled Pork y guacamole", 1.38, 0));
        productList.add(new Product("PULLED PORK y queso brie", 1.38, 0));
        productList.add(new Product("FILETE RUSO, cebolla caramelizada y salsa de queso cheddar"
                , 1.38, 0));
        productList.add(new Product("SALMÓN AHUMADO y crema de queso", 1.38, 0));
        productList.add(new Product("CARNE MECHADA DESHILACHADA y cebolla crujiente", 1.38
                , 0));
        productList.add(new Product("JAMÓN GRAN RESERVA, tomate y aceite de oliva virgen extra", 1.38
                , 0));
        productList.add(new Product("CARRILLERA AL VINO TINTO y queso ibérico", 1.38, 0));
        productList.add(new Product("QUESO IBÉRICO, tortilla de patatas y mayonesa", 1.38
                , 0));
        productList.add(new Product("ALBÓNDIGAS y salsa BBQ", 1.38, 0));
        productList.add(new Product("Pollo, cebolla caramelizada y mayonesa trufada", 1.38
                , 0));
        productList.add(new Product("CHISTORRA, bacon ahumado y salsa brava", 1.38, 0));
        productList.add(new Product("Tortilla de patatas", 1.38, 0));
        genCodProducts();
    }

    public void genCodProducts(){
        int counter = 1;
        for (Product product: productList) {
            product.setCodProduct("p" + counter);
            counter++;
        }
    }

    public Product searchProduct(String code){
        for (Product product: productList) {
            if (product.getCodProduct().equals(code)){
                System.out.print(product);
                return product;
            }
        }
        return null;
    }

    public void listAll(){
        System.out.println(createTable().render(110));
    }

    public AsciiTable createTable(){
        AsciiTable asciiTable = new AsciiTable();
        asciiTable.addRule();
        asciiTable.addRow("Código", null, null, "Descripción", "Precio", "Descuento");
        asciiTable.addRule();
        for (Product product: productList) {
            asciiTable.addRow(product.getCodProduct(), null, null, product.getDescription()
                    , product.getBasePrice() + "€", product.getDiscount() + "%");
            asciiTable.addRule();
        }
        asciiTable.addRow("#", "#", "#", "#", "#", "#");
        asciiTable.setTextAlignment(TextAlignment.RIGHT);
        asciiTable.addRule();
        return asciiTable;
    }
}
