package es.vidal.actividad11;

import java.util.HashMap;
import java.util.Scanner;

public class Login {

    private final Scanner teclado = new Scanner(System.in);

    private final HashMap<String, String> users = new HashMap<>();

    public void fillUsers(){
        users.put("Pedro", "Lacasa");
        users.put("Carlos", "1234");
        users.put("Manolo", "2134");
        users.put("Jordi", "2002");
        users.put("Pepe", "3032");
        users.put("Alex", "7534");
    }

    public boolean verifyUser(){
        for (int i = 0; i < 3; i++) {
            System.out.print("Introduce el Usuario: ");
            String user = teclado.next();
            System.out.print("Introduce la contraseña: ");
            String password = teclado.next();
            if (password.equals(users.get(user))){
                return true;
            }else if (i == 2){
                System.out.println("Lo siento, no tiene acceso al menú del restaurante");
            }else {
                System.out.println("Datos introducidos incorrectos");
            }
        }
        return false;
    }
}

