package es.vidal.actividad11;

public class Product {

    private final double IVA = 0.21;

    private String codProduct;

    private String name;

    private final String description;

    private final double basePrice;

    private final double discount;

    private final double salePrice;

    public Product(String description, double basePrice, double discount) {
        this.description = description;
        this.basePrice = basePrice;
        this.discount = discount;
        salePrice = basePrice + (basePrice * IVA) - (basePrice * (discount / 100));

    }

    @Override
    public String toString() {
        return codProduct + " - " + description + " ";
    }

    public String getCodProduct() {
        return codProduct;
    }

    public double getSalePrice() {
        return salePrice;
    }

    public String getDescription() {
        return description;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public double getDiscount() {
        return discount;
    }

    public void setCodProduct(String codProduct) {
        this.codProduct = codProduct;
    }
}
