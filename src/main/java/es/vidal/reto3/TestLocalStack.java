package es.vidal.reto3;

public class TestLocalStack {
    public static void main(String[] args) {

        String cadena = "Manolo es un hermoso leñador";

        showReverted(cadena);

    }

    public static void showReverted(String cadena){

        LocalStack<Character> data = new LocalStack<>();

        System.out.println("Normal: " + cadena);

        for (int i = 0; i < cadena.length(); i++){
            data.push(cadena.charAt(i));
        }

        System.out.print("Reverse: ");
        while (!data.isEmpty()){
            System.out.print(data.pop());
        }
    }
}
