package es.vidal.reto3;

import java.util.ArrayList;

public class LocalStack <T>{

    ArrayList<T> data = new ArrayList<>();

    public void push(T item){
        data.add(item);
    }

    public T pop(){
        return data.remove(data.size() - 1);
    }

    public boolean isEmpty(){
        return data.isEmpty();
    }
}
