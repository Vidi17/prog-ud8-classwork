package es.vidal.actividad7;

public class Participante {

    private final String nombre;

    private final String dni;

    private final int tiempo;

    public Participante(String nombre, String dni, int tiempo) {
        this.nombre = nombre;
        this.dni = dni;
        this.tiempo = tiempo;
    }
}
