package es.vidal.actividad7;

import java.util.ArrayList;
import java.util.HashSet;

public class Actividad7 {
    public static void main(String[] args) {
        ArrayList<Participante> prueba1 = new ArrayList<>();

        ArrayList<Participante> prueba2 = new ArrayList<>();

        HashSet<Participante> listaParticipantes = new HashSet<>();

        Participante participante1 = new Participante("Toni Garcia", "216798121Q", 12563);

        Participante participante2 = new Participante("Elena Cuenta", "321313123Q", 42156);

        Participante participante3 = new Participante("Maria Perez", "212132342Z", 12342);

        Participante participante4 = new Participante("Juan Magan", "2132123132Z", 67823);

        Participante participante5 = new Participante("Juan Perez", "212381421Q", 4776);

        Participante participante6 = new Participante("Ernesto Cuenta", "351313123Q", 6004);

        Participante participante7 = new Participante("Mari Carmen Ruiz", "112132342Z", 12342);

        Participante participante8 = new Participante("Elena Garcia", "213323132Z", 67823);

        prueba1.add(participante1);
        prueba1.add(participante2);
        prueba1.add(participante3);
        prueba1.add(participante4);

        prueba2.add(participante5);
        prueba2.add(participante6);
        prueba2.add(participante7);
        prueba2.add(participante8);

        llenarHashSet(prueba1, prueba2, listaParticipantes);

    }

    public static void llenarHashSet(ArrayList<Participante> prueba1, ArrayList<Participante> prueba2
            , HashSet<Participante> listaParticipantes){
        listaParticipantes.addAll(prueba1);
        listaParticipantes.addAll(prueba2);
    }
}
