package es.vidal.actividad4;

import java.util.ArrayList;

public class Actividad4 {
    public static void main(String[] args) {
        Videojuego videojuego1 = new Videojuego("Fortnite", "Acción", 40, true);

        Videojuego videojuego2 = new Videojuego("Fifa", "Deportes", 50, true);

        Videojuego videojuego3 = new Videojuego("Grand Theft Auto", "Acción", 80, true);

        Videojuego videojuego4 = new Videojuego("Minecraft", "Simulación", 60, true);

        Videojuego videojuego5 = new Videojuego("Animal Crossing", "Simulación", 30, true);

        ArrayList<Videojuego> listaVideojuegos = new ArrayList<>();

        listaVideojuegos.add(videojuego1);
        listaVideojuegos.add(videojuego2);
        listaVideojuegos.add(videojuego3);
        listaVideojuegos.add(videojuego4);
        listaVideojuegos.add(videojuego5);

        VideoClub videoClub1 = new VideoClub(listaVideojuegos);

        videoClub1.menu();

    }
}
