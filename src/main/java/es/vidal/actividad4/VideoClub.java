package es.vidal.actividad4;

import java.util.ArrayList;
import java.util.Scanner;

public class VideoClub {

    public static final Scanner teclado = new Scanner(System.in);

    ArrayList<Videojuego> listaVideojuegos;

    public VideoClub(ArrayList<Videojuego> listaVideojuegos){
        this.listaVideojuegos = listaVideojuegos;
    }

    public void menu(){
        preguntarUsuario();
    }

    public void preguntarUsuario(){
        System.out.print("Introduzca el nombre del videojuego que desea: ");
        String nombreVideojuego = teclado.next();
        Videojuego videojuego1 = new Videojuego(nombreVideojuego);
        if (compararVideojuegos(videojuego1)){
            System.out.println("Sí disponemos de ese juego");
        }else {
            System.out.println("No disponemos de ese juego");
        }
    }

    public boolean compararVideojuegos(Videojuego videoJuego){
        for (Videojuego listaVideojuego : listaVideojuegos) {
            if (videoJuego.equals(listaVideojuego)) {
                return true;
            }
        }
        return false;
    }
}
