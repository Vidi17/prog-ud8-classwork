//@Author: Jordi Vidal Cerdà//

package es.vidal.actividad4;

import java.util.Arrays;
import java.util.Objects;

public class Videojuego {

    private String titulo;

    private String genero;

    private double precio;

    private boolean multijugador;

    public Videojuego(String titulo, String genero, double precio, boolean multijugador) {
        this.titulo = titulo;
        this.genero = genero;
        this.precio = precio;
        this.multijugador = multijugador;
    }

    public Videojuego(String titulo) {
        this.titulo = titulo;

    }

    public static void sortArraybyPrice(Videojuego[] listaVideojuegos){
        Arrays.sort(listaVideojuegos, Videojuego::compareTo);
    }

    public int compareTo(Videojuego o) {
        return Double.compare(precio, o.precio);
    }


    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setMultijugador(boolean multijugador) {
        this.multijugador = multijugador;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getGenero() {
        return genero;
    }

    public double getPrecio() {
        return precio;
    }

    public boolean isMultijugador() {
        return multijugador;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Videojuego) {
            Videojuego tmpVideojuego = (Videojuego) obj;
            return this.titulo.equals(tmpVideojuego.titulo);
        } else {
            return false;
        }
    }
}
