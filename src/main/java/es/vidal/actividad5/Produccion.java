package es.vidal.actividad5;

public class Produccion {

    private final String titulo;

    private final int duracion;

    private final Formato formato;

    private String resumen;

    private final int fechaLanzamiento;

    private final int precioVenta;

    public Produccion(String titulo, int duracion, int fechaLanzamiento, Formato formato, int precioVenta) {
        this.titulo = titulo;
        this.duracion = duracion;
        this.fechaLanzamiento = fechaLanzamiento;
        this.formato = formato;
        this.precioVenta = precioVenta;
    }

    public String getTitulo() {
        return titulo;
    }

    public String toString(){
        return "\nProducción: título = " + titulo + ", duración = " + getDuracion() + ",\n" +
                "formato = " + formato + ", ";

    }

    public void mostrarDetalle(){
        System.out.printf("%s (%s)\n" +
                "Descripción: %s\n" +
                "Duración: %s\n" +
                "--------------------------------------------\n", titulo, fechaLanzamiento, resumen, getDuracion());
    }

    public int getPrecioVenta() {
        return precioVenta;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public String getDuracion(){
        int calculo = duracion;
        int segundos = 0;
        int minutos = 0;
        int horas = 0;
            do {
                if (calculo >= 3600) {
                    calculo -= 3600;
                    horas = horas + 1;
                } else if (calculo >= 60) {
                    calculo = calculo - 60;
                    minutos = minutos + 1;
                } else {
                    segundos = calculo;
                    calculo -= calculo;
                }
            } while (calculo != 0);
        return horas + "h " + minutos + "min " + segundos + "s";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Produccion){
            Produccion tmpProduccion = (Produccion) obj;
            return this.titulo.equals(tmpProduccion.titulo) && this.formato.equals(tmpProduccion.formato) &&
                    this.precioVenta == tmpProduccion.precioVenta;
        }else {
            return false;
        }
    }
}

