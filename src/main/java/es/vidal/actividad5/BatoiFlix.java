package es.vidal.actividad5;

public class BatoiFlix {
    public static void main(String[] args) {

        Fecha fecha1 = new Fecha(4, 3, 2021);

        Fecha fecha2 = new Fecha(5, 3, 2021);

        Fecha fechaNacimiento1 = new Fecha(4, 3, 2002);

        Fecha fechaNacimiento2 = new Fecha(14, 5, 2001);

        Fecha fechaNacimiento3 = new Fecha(26, 7, 2004);


        Cliente cliente1 = new Cliente(fechaNacimiento1, "mariagarcia@gmail.com", "Maria", "Garcia"
                , "23435423I");

        Cliente cliente2 = new Cliente(fechaNacimiento2, "juanperez@gmail.com", "Juan", "Perez",
                "24614578E");

        Cliente cliente3 = new Cliente(fechaNacimiento3, "elenaramirez@gmail.com", "Elena",
                "Ramírez", "22654257O");


        Pelicula pelicula1 = new Pelicula("News of the world", 3600, fecha1.getAnyo(), Formato.AVI,
                "Tom Hanks", "Carolina Betller");

        Serie serie1 = new Serie("El hacker", 39010, fecha2.getAnyo(), Formato.FLV, 3,
                200);

        Documental documental1 = new Documental("Dream Songs", 4200, fecha2.getAnyo(), Formato.MPG,
                "Enrique Juncosa", "Movimiento Hippie");

        Compra compra1 = new Compra(pelicula1, fecha1, false, "sin comentario");

        Compra compra2 = new Compra(serie1, fecha2, false, "sin comentario");

        Compra compra3 = new Compra(documental1, fecha2, false, "sin comentario");

        Compra compra4 = new Compra(documental1, fecha2, false,
                "Es una pasada de documental");

        Compra compra5 = new Compra(pelicula1, fecha2, false, "sin comentario");

        cliente1.comprarProduccion(compra1);
        cliente1.comprarProduccion(compra2);
        cliente1.comprarProduccion(compra3);
        cliente2.comprarProduccion(compra4);
        cliente3.comprarProduccion(compra5);

        System.out.println(cliente1);
        System.out.println(cliente2);
        System.out.println(cliente3);

    }
}
