package es.vidal.actividad5;

class Fecha {

    private int dia;

    private int mes;

    private int anyo;

    private static final String[] DIAS_TEXTO = new String[] { "lunes", "martes", "miercoles", "jueves", "viernes",
            "sabado", "domingo"};

    private static final String[] MESES_TEXTO = new String[] { "enero", "febrero", "marzo", "abril", "mayo", "junio",
            "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };

    /**
     *  Constructor por defecto
     *  Inicializa una fecha a dia 1-1-1
     */
    public Fecha() {
        this.dia = 1;
        this.mes = 1;
        this.anyo = 1;
    }

    /**
     *  Inicializa la fecha a partir de los parámetros recibidos
     */
    public Fecha(int anyo) {
        this.dia = 0;
        this.mes = 0;
        this.anyo = anyo;
    }

    public Fecha(int dia, int mes, int anyo) {
        this.dia = dia;
        this.mes = mes;
        this.anyo = anyo;
    }

    /**
     * Crea una fecha a partir de otra fecha pasada como argumento
     *
     * Deberemos hacer uso de los métodos consultores para inicializar nuestra clase
     * @param fecha
     */
    public Fecha(Fecha fecha) {
        this.dia = fecha.dia;
        this.mes = fecha.mes;
        this.anyo = fecha.anyo;
    }

    /**
     * Inicializa la fecha a partir de otra pasada en formato String dd/mm/yyyy
     *
     * Deberemos trocearlas de forma que asignemos el día més y año a cada uno de los atributoe
     * @param fecha
     */
    public Fecha(String fecha) {
        String[] numeros = fecha.split("/");
        this.dia = Integer.parseInt(numeros[0]);
        this.mes = Integer.parseInt(numeros[1]);
        this.anyo = Integer.parseInt(numeros[2]);

    }

    /**
     * Modifica la fecha actual a partir de los datos pasados como argumento
     */
    public void set(int dia, int mes, int anyo) {
        this.dia = dia;
        this.mes = mes;
        this.anyo = anyo;
    }

    @Override
    public String toString() {
        StringBuilder fecha = new StringBuilder();
        if (dia < 10){
            fecha.append("0").append(dia);
        }else {
            fecha.append(dia);
        }
        if (mes < 10){
            fecha.append("-0").append(mes);
        }else {
            fecha.append("-").append(mes);
        }
        fecha.append("-").append(anyo);
        return fecha.toString();
    }

    /**
     * Devuelve el día de la semana que representa por la Fecha actual
     * @return @dia
     */
    public int getDia() {
        return dia;
    }

    /**
     * Devuelve el mes que representa la Fecha actual
     * @return @mes
     */
    public int getMes(){
        return mes;
    }

    /**
     * Devuelve el año que representa la Fecha actual
     * @return @mes
     */
    public int getAnyo(){
        return anyo;
    }

    /**
     * Muestra por pantalla la fecha en formato español dd-mm-yyyy
     */
    public void mostrarFormatoES(int dia, int mes, int anyo)  {
        System.out.printf("%02d-%02d-%04d", dia, mes, anyo);
    }

    /**
     * Muestra por pantalla la fecha en formato inglés yyyy-mm-dd
     */
    public void mostrarFormatoGB(int dia, int mes, int anyo) {
        System.out.printf("%04d-%02d-%02d", anyo, mes, dia);
    }

    /**
     * Muestra por pantalla la fecha en formato texto dd-mmmmm-yyyy
     */
    public void mostrarFormatoTexto(int dia, int mes, int anyo) {
        System.out.printf("%d-%s-%04d", dia, MESES_TEXTO[mes-1], anyo);
    }

    /**
     * Retorna un booleano indicando si la fecha del objeto es igual a la fecha pasada como
     * argumento
     *
     * @return boolean
     */
    public boolean isEqual(Fecha otraFecha) {
        if (dia == otraFecha.getDia() && mes == otraFecha.getMes() && anyo == otraFecha.getAnyo()){
            return true;
        }else {
            return false;
        }
    }

    /**
     * Retorna el dia correspondiente de la semana en formato String
     * @return String
     */
    public String getDiaSemana(int dia, int mes, int anyo) {
        int diasTotales = getDiasTranscurridosOrigen(dia, mes, anyo);
        return DIAS_TEXTO[diasTotales % 7];
    }

    /**
     * Solo Festivo sábado o domingo
     * @return boolean
     */
    public boolean isFestivo(int dia, int mes, int anyo) {
        String diaFestivo = getDiaSemana(dia, mes, anyo);
        if ( diaFestivo.equals(DIAS_TEXTO[5]) || diaFestivo.equals(DIAS_TEXTO[6])){
            return true;
        }else {
            return false;
        }
    }

    public Fecha anyadir(int numDias){
        int nuevoDia= dia + numDias;
        int nuevoMes = mes;
        int nuevoAnyo = anyo;
        int numDiasMes = getDiasMes(mes, anyo);
        if (nuevoDia > numDiasMes){
            nuevoDia -= numDiasMes;
            nuevoMes++;
            if (nuevoMes > 12){
                nuevoMes = 1;
                nuevoAnyo++;
            }
        }
        return new Fecha(nuevoDia, nuevoMes, nuevoAnyo);
    }

    public Fecha restar(int numDias){
        int nuevoDia = dia - numDias;
        int nuevoMes = mes;
        int nuevoAnyo = anyo;
        int numDiasMes = getDiasMes(mes-1, anyo);
        if (nuevoDia <= 0){
            nuevoMes--;
            nuevoDia += numDiasMes;
            if (nuevoMes <= 0){
                nuevoMes = 12;
                nuevoAnyo--;
            }
        }
        return new Fecha(nuevoDia, nuevoMes, nuevoAnyo);
    }

    public boolean isCorrecta(int dia, int mes, int anyo){
        if (dia < 1 || dia > getDiasMes(mes, anyo) || mes < 1 || mes > 12 ){
            return false;
        }else {
            return true;
        }
    }

    /**
     * Retorna el mes correspondiente de la semana en formato caracter
     * @return char
     */
    private String getMesTexto(int mes) {
        return MESES_TEXTO[mes-1];
    }

    /**
     * Devuelve el número de dias transcurridos desde el 1-1-1
     *
     * @return int
     */
    private int getDiasTranscurridosOrigen(int dia, int mes, int anyo) {
        int diasTotales = getDiasTranscurridosAnyo(dia, mes, anyo);
        for (int i = 2; i < anyo; i++) {
            diasTotales += getDiasAnyo(anyo);
        }
        return diasTotales;
    }

    /**
     * Devuelve el número de dias transcurridos en el anyo actual
     *
     * @return int
     */
    private int getDiasTranscurridosAnyo(int dia, int mes, int anyo) {
        int diasTotales = dia;
        for (int i = 1; i < mes; i++) {
            diasTotales += getDiasMes(i, anyo);
        }
        return diasTotales;
    }

    /**
     * Indica si el año pasado como argumento es bisiesto
     * Un año es bisiesto si es divisible por 4 a su vez 100 por 400
     *
     * @return boolean
     */
    private boolean isBisiesto(int anyo){
        if (anyo % 4 == 0 && anyo % 100 == 0 && anyo % 400 == 0){
            return true;
        }
        return false;
    }

    /**
     *  Calcula el número de días que tiene el mes representado por la fecha actual
     *
     *  @return int total dias mes en curso
     */
    private int getDiasMes(int mes, int anyo) {
        if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10  || mes == 12 || mes == 0){
            return 31;
        }else if (mes == 2 && isBisiesto(anyo)){
            return 29;
        }else if (mes == 2 && !isBisiesto(anyo)){
            return 28;
        }else {
            return 30;
        }
    }

    /**
     * Calcula el número total de dias que tiene el año pasado como argumento
     *
     * @return int total dias anyo en curso
     */
    public int getDiasAnyo(int anyo){
        if (isBisiesto(anyo)){
            return 366;
        }else {
            return 365;
        }
    }
}
