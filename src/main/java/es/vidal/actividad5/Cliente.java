package es.vidal.actividad5;

import java.util.ArrayList;

public class Cliente {

    private final Fecha fechaNacimiento;

    private final String email;

    private final String nombre;

    private final String apellidos;

    private final String dni;

    private ArrayList<Compra> listadoCompras = new ArrayList<>();

    private int totalDineroGastado;

    public Cliente(Fecha fechaNacimiento, String email, String nombre, String apellidos, String dni) {
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
    }

    public void comprarProduccion(Compra compra){
        if (!comprobarProduccion(compra)){
            compra.setCliente(this);
            listadoCompras.add(compra);
            totalDineroGastado += compra.getProduccion().getPrecioVenta();
            System.out.println("La compra se ha añadido correctamente");
        }else {
            System.out.println("La producción de dicha compra ya ha sido registrada, por favor introduzca otra compra");
        }
    }

    public boolean comprobarProduccion(Compra compra){
        return listadoCompras.contains(compra);
    }

    @Override
    public String toString() {
        return "Cliente: " + nombre + " " + apellidos + " | Compras(" + listadoCompras.size() + "): " +
                mostrarCompras() + " - Total Gastado: " + totalDineroGastado + "€";
    }

    public String mostrarCompras(){
        StringBuilder compras = new StringBuilder();
        compras.append("[").append(listadoCompras.get(0));
        for (int i = 1; i < listadoCompras.size(); i++) {
            compras.append(", ");
            compras.append(listadoCompras.get(i));
        }
        compras.append("]");
        return compras.toString();
    }
}
