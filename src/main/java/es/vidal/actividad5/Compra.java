package es.vidal.actividad5;

public class Compra {

    private final Produccion produccion;

    private final Fecha fecha;

    private Cliente cliente;

    private final boolean visualizacionFinalizada;

    private final String comentario;

    public Compra(Produccion produccion, Fecha fecha, boolean visualizacionFinalizada,
                  String comentario) {
        this.produccion = produccion;
        this.fecha = fecha;
        this.visualizacionFinalizada = visualizacionFinalizada;
        this.comentario = comentario;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Produccion getProduccion() {
        return produccion;
    }

    @Override
    public String toString() {
        return produccion.getTitulo() + " - " + fecha + " - " + comentario;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Compra){
            Compra tmpCompra = (Compra) obj;
            return this.produccion.equals(tmpCompra.produccion) && this.fecha.equals(tmpCompra.fecha) &&
                    this.cliente.equals(tmpCompra.cliente) && this.visualizacionFinalizada
                    == tmpCompra.visualizacionFinalizada;
        }else {
            return false;
        }
    }
}
