package es.vidal.actividad5;

public class Pelicula extends Produccion{

    private String actorPrincipal;

    private String actriz;

    public Pelicula(String titulo, int duracion, int fechaLanzamiento, Formato formato, String actorPrincipal, String actriz) {
        super(titulo, duracion, fechaLanzamiento, formato, 12);
        this.actorPrincipal = actorPrincipal;
        this.actriz = actriz;
    }

    public Pelicula(String titulo, Formato formato, int duracion, String actriz,int fechaLanzamiento){
        super(titulo,duracion,fechaLanzamiento, formato, 12);
        this.actriz = actriz;
    }

    public Pelicula(String titulo, Formato formato, String actriz,int fechaLanzamiento){
        super(titulo, 4800,fechaLanzamiento, formato, 12);
        this.actriz = actriz;
    }

    public Pelicula(String titulo, Formato formato, String actorPrincipal, int duracion,int fechaLanzamiento){
        super(titulo,duracion,fechaLanzamiento, formato, 12);
        this.actorPrincipal = actorPrincipal;
    }

    public Pelicula(String titulo, String actorPrincipal, Formato formato,int fechaLanzamiento){
        super(titulo,4800,fechaLanzamiento, formato, 12);
        this.actorPrincipal = actorPrincipal;
    }

    public Pelicula(String titulo, int fechaLanzamiento, Formato formato, String actorPrincipal, String actriz) {
        super(titulo, 4800, fechaLanzamiento, formato, 12);
        this.actorPrincipal = actorPrincipal;
        this.actriz = actriz;
    }

    @Override
    public String toString(){
        return super.toString() +
                "(Película) actor = " + actorPrincipal + ", actriz = " + actriz;
    }

    @Override
    public void mostrarDetalle(){
        System.out.print("---------------- Película ---------------\n");
        super.mostrarDetalle();
    }
}
