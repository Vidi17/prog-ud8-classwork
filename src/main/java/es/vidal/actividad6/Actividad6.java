package es.vidal.actividad6;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class Actividad6 {

    public static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {
        HashSet<String> personas = new HashSet<>();

        insertarPersonas(personas);
        mostrarPersonas(personas);


    }

    public static void insertarPersonas(HashSet<String> personas){
        String[] nombres;
        System.out.println("Inserta los nombres de las personas, acaba con un Fin para finalizar:");
        do {
            nombres = teclado.nextLine().split(" ");
            for (String nombre : nombres) {
                if (!nombre.equalsIgnoreCase("Fin")) {
                    personas.add(nombre);
                }
            }
        }while (!nombres[nombres.length - 1].equalsIgnoreCase("fin"));
    }

    public static void mostrarPersonas(HashSet<String> personas){
        Iterator<String> nombresIterator = personas.iterator();
        while (nombresIterator.hasNext()){
            String nombre = nombresIterator.next();
            System.out.print(nombre + ", ");
        }
    }
}
