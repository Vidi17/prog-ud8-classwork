package es.vidal.actividad10;

import java.util.HashMap;
import java.util.Scanner;

public class Actividad10 {

    public static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {
        HashMap<String, String> usuarios = new HashMap<>();

        usuarios.put("Pedro", "Lacasa");
        usuarios.put("Carlos", "1234");
        usuarios.put("Manolo", "2134");

        System.out.print("Introduce el Usuario: ");
        String nombreUsuario = teclado.next();

        inicioDeSesion(nombreUsuario, usuarios);

    }

    public static void inicioDeSesion(String usuario, HashMap<String, String> usuarios){
        for (int i = 0; i < 3; i++) {
            System.out.print("Introduce la contraseña: ");
            String contrasenya = teclado.next();
            if (contrasenya.equals(usuarios.get(usuario))){
                System.out.println("Ha accedido al área restringida");
                return;
            }else if (i == 2){
                System.out.println("Lo siento, no tiene acceso al área restringida");
            }
        }
    }
}
