package es.vidal.actividad9;

import java.util.*;

public class TestDiccionario {

    public static void main(String[] args) {
        HashMap<String, String> traductor = new HashMap<>();

        traductor.put("Gato", "Cat");
        traductor.put("Perro", "Dog");
        traductor.put("Casa", "House");
        traductor.put("Pajaro", "Bird");
        traductor.put("Negro", "Black");
        traductor.put("Azul", "Blue");
        traductor.put("Piedra", "Stone");
        traductor.put("Rojo", "Red");
        traductor.put("Red", "Net");
        traductor.put("Jugar", "Play");
        traductor.put("Mochila", "Backpack");
        traductor.put("España", "Spain");
        traductor.put("Cara", "Face");
        traductor.put("Carlos", "Charles");
        traductor.put("Mejor", "Better");
        traductor.put("Coche", "Car");
        traductor.put("Puerta", "Door");
        traductor.put("Pantalla", "Screen");
        traductor.put("Teclado", "Keyboard");
        traductor.put("Pc", "Computer");

        Diccionario diccionario1 = new Diccionario(traductor);

        JuegoDiccionario juego1 = new JuegoDiccionario(diccionario1);

        System.out.printf("\nHas acertado %d palabras", juego1.traducirPalabras());
    }
}
