package es.vidal.actividad9;

import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;

public class JuegoDiccionario {

    private final Scanner teclado = new Scanner(System.in);

    private final Diccionario diccionario;

    public JuegoDiccionario(Diccionario diccionario) {
        this.diccionario = diccionario;
    }

    public HashSet<String> escojer5Palabras(){
        Random aleatorio = new Random();
        String[] palabrasAlAzar = diccionario.getListaPalabras().keySet().toArray(new String[0]);
        HashSet<String> palabrasAAdivinar = new HashSet<>();
        do {
            palabrasAAdivinar.add(palabrasAlAzar[aleatorio.nextInt(20)]);
        }while (palabrasAAdivinar.size() != 5);
        return palabrasAAdivinar;
    }

    public int traducirPalabras(){
        HashSet<String> palabrasATraducir = escojer5Palabras();
        int aciertos = 0;
        for (String palabra : palabrasATraducir) {
            System.out.println("Traduce: " + palabra);
            String palabraUsuario = teclado.next();
            if (diccionario.getListaPalabras().get(palabra).equals(palabraUsuario)) {
                aciertos++;
            }
        }
        return aciertos;
    }
}
