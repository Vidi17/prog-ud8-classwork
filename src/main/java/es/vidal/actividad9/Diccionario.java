package es.vidal.actividad9;

import java.util.HashMap;


public class Diccionario {

    private final HashMap<String, String> listaPalabras;

    public Diccionario(HashMap<String, String> listaPalabras) {
        this.listaPalabras = listaPalabras;
    }

    public HashMap<String, String> getListaPalabras() {
        return listaPalabras;
    }
}
