package es.vidal.actividad1;

import java.util.ArrayList;
import java.util.Scanner;

public class Actividad1 {

    public static final Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {
        ArrayList<String> colores = new ArrayList<>();

        colores.add("Rojo");
        colores.add("Naranja");
        colores.add("Amarillo");
        colores.add("Verde");
        colores.add("Azul");
        colores.add("Añil");
        colores.add("Violeta");

        System.out.println(colores.size());

        mostrarPosicionColor(colores);

        for (String color : colores) {
            System.out.println(color);
        }

        mostrarSiEstaColor(colores);
    }

    public static void mostrarPosicionColor(ArrayList<String> colores){
        if (colores.contains("Rojo")){
            System.out.println(colores.indexOf("Rojo"));
        }
    }

    public static void mostrarSiEstaColor(ArrayList<String> colores){
        String color = obtenerColor();
        if (colores.contains(color)){
            System.out.println("Si que se encuentra");
        }else {
            colores.add(color);
        }
    }

    public static String obtenerColor(){
        String color;
        System.out.println("Introduce un color");
        color = teclado.next();
        return color;
    }
}
